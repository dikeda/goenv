// Copyright 2012 Yoshifumi Yamaguchi, All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

const Version = "0.4.2"

var (
	version bool
	goroot  string
	deps    string
	gae     bool
)

// usage shows goenv command usage and command options.
func usage() {
	versionText := "goenv -- Go language environment manager (ver. " + Version + ")"
	usageText := `
usage: goenv [-go <GOROOT>] [-deps <additonal GOPATH>] path
       goenv -version

options:
  -go:		specify GOROOT
  -deps:	add dependent package paths in GOPATH
`
	fmt.Println(versionText + usageText)
}

// readLines returns strings separated by newline character.
func readLines(r io.Reader) ([]string, error) {
	var (
		err    error
		part   []byte
		prefix bool
	)
	reader := bufio.NewReader(r)
	buffer := bytes.NewBuffer(make([]byte, 0))
	lines := []string{}
	for {
		if part, prefix, err = reader.ReadLine(); err != nil {
			break
		}
		buffer.Write(part)
		if !prefix {
			lines = append(lines, buffer.String())
			buffer.Reset()
		}
	}
	if err == io.EOF {
		err = nil
	} else {
		return nil, err
	}
	return lines, nil
}

// doExists confirms whether provided directory exists or not.
func doExists(dir string) error {
	_, err := os.Stat(dir)
	if err != nil {
		return err
	}
	return nil
}

// ynQuestion put yes/no prompt on the terminal.
func ynQuestion(q string) bool {
	fmt.Printf("%s", q)
	var input string
	for {
		fmt.Scanf("%s", &input)
		switch input {
		case "y", "Y":
			return true
		case "n", "N", "":
			return false
		default:
			fmt.Printf("answer in 'y' or 'n'. your answer: %#v\n", input)
		}
	}
	return false
}

// createScript generate script with template text and a struct inside envhome.
func createScript(scriptPath string, t *template.Template, p interface{}) (*os.File, error) {
	file, err := os.Create(scriptPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	err = t.Execute(file, p)
	if err != nil {
		return nil, err
	}
	err = file.Chmod(os.FileMode(0755))
	if err != nil {
		return nil, err
	}

	return file, nil
}

// createGAEFiles creates app.yaml and <package name>.go file for GAE
func createGAEFiles(envpath, envname, filename string, t *template.Template) error {
	parameters := struct {
		EnvName string
	}{
		EnvName: envname,
	}
	scriptPath := filepath.Join(filepath.Join(envpath, "src"), filename)
	_, err := createScript(scriptPath, t, parameters)
	if err != nil {
		return err
	}
	return nil
}

// makeEnv creates `src` directory inside envpath and generate an `activate` script
// under envhome.
func makeEnv(envpath, goroot, deps string) (string, error) {
	var err error
	if goroot != "" {
		if err := doExists(goroot); err != nil {
			return "", err
		}
	}

	// make envhome dir
	var absEnvPath string
	trimedPath := strings.TrimLeft(envpath, " ")
	if filepath.IsAbs(trimedPath) {
		absEnvPath = filepath.Clean(trimedPath)
	} else {
		absEnvPath, err = filepath.Abs(trimedPath)
		if err != nil {
			return "", err
		}
	}
	envname := filepath.Base(envpath)
	if doExists(absEnvPath) == nil {
		question := fmt.Sprintf("Directory '%s' exist.\nDo you want overwrite it? [y/N]: ", absEnvPath)
		if !ynQuestion(question) {
			return "", err
		}
	}

	if doExists(filepath.Dir(absEnvPath)) != nil {
		question := fmt.Sprintf("Do you want to create all parental directory of '%s'? [y/N]: ", absEnvPath)
		if !ynQuestion(question) {
			return "", err
		}
	}

	gopath := absEnvPath
	if deps != "" {
		gopath = deps + ":" + gopath
	}

	// make `src` directory under envhome
	sourceDir := filepath.Join(absEnvPath, "src")
	if err := os.MkdirAll(sourceDir, os.FileMode(0775)); err != nil {
		return "", err
	}

	// generate `activate` script
	parameters := &struct {
		PS1     string
		GOPATH  string
		GOROOT  string
		ENVNAME string
		GAE     bool
	}{
		PS1:     "(go:" + envname + ") $_OLD_PS1",
		GOPATH:  gopath,
		ENVNAME: envname,
		GAE:     gae,
	}

	if goroot != "" {
		parameters.GOROOT = goroot
	}

	scriptPath := filepath.Join(absEnvPath, "activate")
	_, err = createScript(scriptPath, activateTemplate, parameters)
	if err != nil {
		return "", err
	}

	// GAE/Go
	if gae {
		err = createGAEFiles(absEnvPath, envname, "app.yaml", appYamlTemplate)
		err = createGAEFiles(absEnvPath, envname, envname+".go", appGoTemplate)
		if err != nil {
			return "", err
		}
	}

	return absEnvPath, nil
}

func init() {
	flag.BoolVar(&version, "version", false, "Show version")
	flag.StringVar(&goroot, "go", "", "Specify GOROOT")
	flag.StringVar(&deps, "deps", "", "Add dependent package paths in GOPATH")
	flag.BoolVar(&gae, "gae", false, "Create Google App Engine for Go workspace")
}

func main() {
	flag.Parse()

	if version {
		usage()
		return
	}

	// normal goenv command
	if flag.NArg() == 0 {
		fmt.Println("Error: assign envname for goenv\n")
		usage()
		return
	}

	envpath, err := makeEnv(flag.Arg(0), goroot, deps)
	if err != nil {
		log.Fatalf("[goenv] %v\n", err)
	}
	if len(envpath) > 0 {
		fmt.Printf("Environment %s created!\n", envpath)
	}
}
